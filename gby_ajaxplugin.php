<?php
/*
Plugin Name: Ajax Plugin
Plugin URI: http://www.softwareseni.com
Description: This plugin uses ajax for retrieving and showing data.
Version: 1.0
Author: Prisantya
Author URI: http://www.softwareseni.com
License: GPL2
*/

add_shortcode('wp6_training','gby_ajaxplugin');
add_action('wp_enqueue_scripts', 'gby_addjs' );  
add_action('wp_ajax_ajaxplugin','gby_autocomplete');
add_action('wp_ajax_nopriv_ajaxplugin','gby_autocomplete');

function gby_ajaxplugin(){
    ob_start();
    gby_form();
    return ob_get_clean();
}


function gby_form() {
    $search = (isset($_GET['searchpost']) && $_GET['searchpost']) ? $_GET['searchpost'] : '';
?>
    <div class="wrap">
        <h2>Search</h2>
        <div class="ui-widget">
        <form class="searchForm" method="get" action="<?php echo esc_url($_SERVER['REQUEST_URI']);?>">
        <label for="search"> Sarch post here </label>
        <input id="search" type="text" name="searchpost" class="ui-widget-content" value="<?php echo $search; ?>" size="40" />
        <input type="submit" name="search-submit" value="Submit">
        </form>
        </div>
    </div>
<?php

    if (isset($_GET) && isset($_GET['searchpost'])) {
        if (!empty($_GET['search-submit'])) {
            echo gby_tampil($_GET['searchpost']);
        }
    }
}

function gby_addjs() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('ajaxjs',plugin_dir_url(__FILE__).'js/frontend.js', array('jquery-ui-autocomplete','jquery'));
    wp_localize_script('ajaxjs','ajax_object', array('ajax_url'=>admin_url('admin-ajax.php')));

    wp_register_style( 'jquery-ui-styles','http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' );
	wp_enqueue_style( 'jquery-ui-styles' );
    
}

function gby_autocomplete(){
    global $wpdb;

    $posts=array();  

    $arg = [
        'query_label' => 'by_title',
        'post_type' => 'post',
        'post_status' => 'publish',
        's' => $_GET['searchpost'] 
    ];

    $posts=array();
    /**
    add_filter('posts_where', function ($where, $query) {
        $label = $query->query['query_label'] ?? '';
	    if($label === 'by_title') {
            global $wpdb;
		    $where .= 'AND '.$wpdb->base_prefix.'posts.post_title like "%'.$_GET['searchpost'].'%"';
	    }

	return $where;
    }, 10, 2);
    */
    
    $results = new WP_Query($arg); 
    $i=0; 
    
    foreach ($results->posts as $result) {
        $posts['title'.$i]=$result->post_title;
        $i++;
    }
    
         wp_reset_postdata();
        echo json_encode($posts);
    die();
}

function gby_tampil($keyword) {
    global $wpdb;
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
  
     $arg = [
           'query_label' => 'exception',
           'post_type' => 'post',
           'post_status' => 'publish',
           'posts_per_page' => 2,
           'paged' => $paged,
           's' => $keyword, //get_search_query(),
           'order_by'=>'asc'
    ];

    /**
    add_filter('posts_where', function ($where, $query) use ($keyword) {
        $label = $query->query['query_label'] ?? '';
	    if($label === 'exception') {
            global $wpdb;
		    $where .= 'AND '.$wpdb->base_prefix.'posts.post_title!="'.$keyword.'"';
	    }

	return $where;
    }, 10, 2);
    */

    $results = new WP_Query($arg);
    foreach ($results->posts as $result) {
         echo '<div>' . $result->post_title . '</div>';
        
    }
    wp_reset_postdata();
    
    $big = 999999999; 

            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $results->max_num_pages
            ) );
}
