jQuery(document).ready(function($) {
  
      $( "#search" ).autocomplete({
        source: function(request, response) {
            $.ajax({
				type: 'GET',
				dataType: 'json',
				url: ajax_object.ajax_url,
				data: 'action=ajaxplugin&searchpost='+request.term,
				success: function(data) {
					response(data);
				}
			});
        }
      });
})